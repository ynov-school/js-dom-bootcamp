
document.body.onload = addContent;

function addContent () {
    const contentDiv = document.createElement('div');
    contentDiv.id = 'content';

    const menuDiv = document.createElement('div')
    menuDiv.className = 'menu dark-grey'
    document.body.appendChild(contentDiv).appendChild(menuDiv)

    const targetsDiv = document.createElement('div')
    targetsDiv.className = 'targets grey'
    document.body.appendChild(contentDiv).appendChild(targetsDiv)

    for (i=0; i < 5; i++) {
        const itemsDiv = document.createElement('div')
        itemsDiv.className = 'item light-grey'
        menuDiv.append(itemsDiv)
        const h2 = document.createElement('h2')
        itemsDiv.append(h2)
        h2.innerHTML = i
        const span = document.createElement('span')
        itemsDiv.append(span)

        if (i == 0 || i == 3) {
            span.innerHTML = 'cible 1'
        } else {
            span.innerHTML = 'cible 2'
        }
    }

    for (i=0; i < 2; i++) {
        const targetDiv = document.createElement('div')
        targetsDiv.append(targetDiv)
        const h3 = document.createElement('h3')
        targetDiv.append(h3)
        
        if (i==0) {
            targetDiv.className = 'target pink'
            h3.innerHTML = 'cible 1'
            
        } else {
            targetDiv.className = 'target red'
            h3.innerHTML = 'cible 2'
        }

        const span = document.createElement('span')
        span.classList.add('compteur')
        targetDiv.append(span)
        span.innerHTML = 0
    }

}