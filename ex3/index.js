const DRAGGABLE_ELEMENTS_WIDTH = 100
const DRAGGABLE_ELEMENTS_HEIGHT = 100

let currentContentWidth = null
let currentContentHeight = null

document.addEventListener('DOMContentLoaded', () => {
    onResize() // to initialize currentContentWidth / currentContentHeight
    renderDraggableElements()
    attachDragEvents()
})

function attachDragEvents() {
    //-- Exercice principal : Implémentez le drag and drop
    const contentElement = document.getElementById('content')
    contentElement.addEventListener('pointerdown', userPressed, {passive: true})

    function userPressed(event) {
        element = event.target
        if (element.classList.contains('draggableBox')) {
            let zIndexes = []
            document.querySelectorAll('.draggableBox').forEach((box) => {
                zIndexes.push(box.style.zIndex);
            })
            element.style.zIndex = Math.max(...zIndexes) + 1
            startX = event.clientX;
            startY = event.clientY;
            bbox = element.getBoundingClientRect()
            contentElement.addEventListener("pointermove", userMoved, {
                passive: true
            })
            contentElement.addEventListener('pointerup', userReleased, {
                passive: true
            })
            contentElement.addEventListener('pointercancel', userReleased, {
                passive: true
            })

        }

        function userMoved(event) {
            let deltaX = event.clientX - startX;
            let deltaY = event.clientY - startY;
            element.style.left = bbox.left + deltaX + "px";
            element.style.top = bbox.top + deltaY + "px";
        }

        function userReleased(event) {
            contentElement.removeEventListener("pointermove", userMoved);
            contentElement.removeEventListener("pointerup", userReleased);
            contentElement.removeEventListener("pointercancel", userReleased);
        }
    }
}

function renderDraggableElements() {
    const contentElement = document.getElementById('content')
    const maxLeft = currentContentWidth - DRAGGABLE_ELEMENTS_WIDTH
    const maxTop = currentContentHeight - DRAGGABLE_ELEMENTS_HEIGHT

    for (let i = 0; i <= 10; i++) {
        const divElement = document.createElement('div')
        divElement.className = 'draggableBox'
        divElement.appendChild(document.createTextNode(`Box nº${i}`))
        divElement.style.left = Math.floor(Math.random() * maxLeft) + 'px'
        divElement.style.top = Math.floor(Math.random() * maxTop) + 'px'
        contentElement.appendChild(divElement)
    }

}

window.addEventListener('optimizedResize', onResize)

function onResize() {
    const contentElement = document.getElementById('content')

    //-- Exercice Bonus 3: implémenter ici le repositionnement des box lorsque la fenêtre change de taille, les box doivent proportionnellement se retrouver à la même place

    currentContentWidth = contentElement.offsetWidth
    currentContentHeight = contentElement.offsetHeight
}

// See https://developer.mozilla.org/en-US/docs/Web/Events/resize
// Prevent resize event to be fired way too often, this means neither lags nor freezes
{
    function throttle(type, name, obj = window) {
        let running = false
        const event = new CustomEvent(name)
        obj.addEventListener(type, () => {
            if (running) return
            running = true
            requestAnimationFrame(() => {
                obj.dispatchEvent(event)
                running = false
            })
        })
    }

    /* init - you can init any event */
    throttle('resize', 'optimizedResize');
}